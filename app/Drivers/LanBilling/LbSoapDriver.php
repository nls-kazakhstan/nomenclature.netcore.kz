<?php

namespace App\Drivers\LanBilling;

use SoapClient;

class LbSoapDriver
{
    /**
     * @var \SoapClient
     */
    protected $soapClient;

    /**
     * @param bool $adminAuthentication
     * @throws \SoapFault
     */
    public function __construct(bool $adminAuthentication = true)
    {
        $wsdl = env("LB_WSDL_URL");

        $this->soapClient = new SoapClient($wsdl, [
            'user_agent' => 'PHP-SOAP/php-version',
            'connection_timeout' => 5,
            'cache_wsdl' => true,
            'trace' => 1,
            'stream_context' => stream_context_create([
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ],
            ]),
        ]);

        $this->soapClient->__setLocation(env('LB_WSDL_URL_ENDPOINT'));

        if ($adminAuthentication === true) {
            $this->loginAsAdmin();
        }
    }

    /**
     * @return void
     */
    protected function loginAsAdmin(): void
    {
        $this->soapCall('login', [
            'login' => env('LB_ADMIN_USERNAME'),
            'pass' => env('LB_ADMIN_PASSWORD'),
        ]);
    }

    /**
     * @param string $method
     * @param array $options
     * @return mixed|null
     */
    protected function soapCall(string $method, array $options)
    {
        return optional($this->soapClient->$method($options))->ret;
    }
}
