<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserNomenclature extends Model
{
    use HasFactory;

    protected $fillable = ['request_id', 'lb_account_id', 'lb_agreement', 'iin',
        'tariff_id', 'nomenclature_id', 'payment_type_id',
        'count', 'price_id', 'synced_at', 'archived_at'];

}
