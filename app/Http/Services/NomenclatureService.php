<?php

namespace App\Http\Services;

use App\Models\BitrixId;
use App\Models\LbId;
use App\Models\LoanPlan;
use App\Models\Nomenclature;
use App\Models\NomenclaturePrice;
use App\Models\NomenclatureRequest;
use App\Models\PaymentType;
use App\Models\Tariff;
use App\Models\UserNomenclature;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class NomenclatureService
{
    public function create($lb_id, $lb_agreement, $iin, $tariff, $nomenclatures): array
    {
        UserNomenclature::query()
            ->where('lb_account_id', $lb_id)
            ->where('lb_agreement', $lb_agreement)
            ->whereNull('synced_at')
            ->whereNull('archived_at')
            ->update(['archived_at' => now()]);

        $nomenclature_request = NomenclatureRequest::query()->create();
        $tariff = Tariff::query()->where('name', $tariff)->first();
        $tariff_price = NomenclaturePrice::query()->where('tariff_id', $tariff->id)->first();
        $add_tariff = UserNomenclature::query()->create([
            'request_id' => $nomenclature_request['id'],
            'lb_account_id' => $lb_id,
            'lb_agreement' => $lb_agreement,
            'iin' => $iin,
            'tariff_id' => $tariff->id,
            'count' => 1,
            'price_id' => $tariff_price->id,
        ]);

        $response = [
            'error' => 0,
            'request_id' => $nomenclature_request['id'],
            'details' => [
                'tariff' => [
                    'id' => $add_tariff['id'],
                    'tariff' => $tariff->title,
                    'price' => $tariff_price->price,
                ],
                'services' => [],
            ],
        ];

        foreach ($nomenclatures as $nomenclature) {
            $payment_type = (Str::is('loan*', $nomenclature['payment_type'])) ? 'purchase' : $nomenclature['payment_type'];
            $nomenclature_data = Nomenclature::query()->where('id', $nomenclature['nomenclature_id'])->first();
            $payment_data = PaymentType::query()->where('name', $payment_type)->first();
            $price_data = NomenclaturePrice::query()
                ->where('nomenclature_id', $nomenclature['nomenclature_id'])
                ->where('payment_type_id', $payment_data->id)
                ->first();
            $payment_type_id = PaymentType::query()->where('name', $nomenclature['payment_type'])->first();
            $add_nomenclature = UserNomenclature::query()->create([
                'request_id' => $nomenclature_request['id'],
                'lb_account_id' => $lb_id,
                'lb_agreement' => $lb_agreement,
                'iin' => $iin,
                'tariff_id' => $tariff->id,
                'nomenclature_id' => $nomenclature_data->id,
                'payment_type_id' => $payment_type_id['id'],
                'count' => $nomenclature['count'],
                'price_id' => $price_data->id,
            ]);
            array_push($response['details']['services'], [
                'id' => $add_nomenclature['id'],
                'nomenclature' => $nomenclature_data->title,
                'count' => $nomenclature['count'],
                'payment_type' => $payment_type_id['title'],
                'price' => $price_data->price,
            ]);
        }
        $answer = $this->sendBitrix($nomenclature_request['id']);

        return $response;
    }

    public function activate($lb_account_id, $lb_agreement, $request_id, $tv24_synchronize, $tv24_username): array
    {
        return $this->sendLb($request_id, $lb_account_id, $lb_agreement, $tv24_synchronize, $tv24_username);
    }

    private function sendBitrix($request_id)
    {
        $nomenclatures = $this->GetNomenclatures($request_id, null, null);
        $dataCRM = ["client_iin" => $nomenclatures[0]->iin, "uid" => "1"];

        foreach ($nomenclatures as $nomenclature) {
            if (blank($nomenclature->nomenclature_id)) {
                $tariffCrmId = BitrixId::query()->where('tariff_id', $nomenclature->tariff_id)->whereNull('nomenclature_id')->first();
                $dataCRM["services"][$tariffCrmId->bitrix_id] = $nomenclature->count;
            } else {
                $serviceCrmId = BitrixId::query()->whereNull('tariff_id')->where('nomenclature_id', $nomenclature->nomenclature_id)->first();
                $dataCRM["services"][$serviceCrmId->bitrix_id] = $nomenclature->count;
            }
        }

        $url = env("CRM_URL")."/api/mobile/mobile-update-data.php?request={\"0\":".json_encode($dataCRM)."}";
        //        dd($url);
        $response = Http::withoutVerifying()->get($url);

        //        dd($response->json());
        return $response->json();
    }

    private function sendLb($request_id, $lb_account_id, $lb_agreement, $tv24_synchronize, $tv24_username): array
    {
        $nomenclatures = $this->GetNomenclatures($request_id, $lb_account_id, $lb_agreement);

        if ($nomenclatures->count() == 0) {
            return ['error' => 1, 'errorText' => "request not found"];
        }

        $LBService = new LBService();
        $vgroup = $LBService->getVgroups($nomenclatures[0]->lb_account_id);
        //        dd($vgroup);
        $tariffLbId = 0;
        $new_tariff = 0;

        foreach ($nomenclatures as $nomenclature) {
            if (blank($nomenclature->nomenclature_id)) {
                $tariffLb = LbId::query()->where('tariff_id', $nomenclature->tariff_id)->whereNull('nomenclature_id')->first();
                $tariffLbId = $tariffLb['lb_id'];
                $new_tariff = $LBService->createTariffRasp($tariffLbId, $vgroup->vgid);
                //                dd($new_tariff);
                if ($new_tariff) {
                    UserNomenclature::query()->find($nomenclature->id)->update(['synced_at' => now()]);
                }
            } else {
                $serviceLb = LbId::query()
                    ->where('nomenclature_id', $nomenclature->nomenclature_id)
                    ->where('payment_type_id', $nomenclature->payment_type_id)
                    ->whereNull('tariff_id')
                    ->first();
                $price = NomenclaturePrice::query()->find($nomenclature->price_id);
                $payment_type = PaymentType::query()->find($nomenclature->payment_type_id);
                //                var_dump($nomenclature->id, $nomenclature->price_id);
                $serviceCat = $LBService->getService($tariffLbId, $serviceLb->lb_title);
                //                dd($serviceCat);

                if ($tariffLbId > 0 && $vgroup->vgid > 0 && $serviceLb['lb_id'] > 0 && $new_tariff > 0) {
                    //Create loan plan
                    if (Str::is('loan*', $payment_type['name'])) {
                        //                    echo $serviceLb['lb_title'] . ' ' . $payment_type['name']. ' ' . $payment_type['sub_count'].'<br>';
                        $once_pay = $price['price'] / $payment_type['sub_count'];
                        for ($i = 1; $i <= $payment_type['sub_count']; $i++):
                            LoanPlan::query()->create([
                                'user_nomenclature_id' => $nomenclature->id,
                                'payment_date' => now()->addMonths($i),
                                'payment_amount' => $once_pay,
                            ]);
                            $service_id = $LBService->createService($vgroup->vgid, $tariffLbId, $serviceCat->catidx, $once_pay, 1, $serviceLb['service_type'], now()->addMonths($i)->toDateTimeString());
                        endfor;
                    } else {
                        $service_id = $LBService->createService($vgroup->vgid, $tariffLbId, $serviceCat->catidx, $price['price'], $nomenclature->count, $serviceLb['service_type'], now()->addMinute()->toDateTimeString());
                    }
                    //                    echo $vgroup->vgid . " " . $tariffLbId . " " . $serviceLb['lb_id'] .
                    //                        " " . $price['price'] . " " . $nomenclature->count . " " . $new_tariff . " " . $serviceCat->catidx . "<br>";

                    //                    $service_id = $LBService->createService($vgroup->vgid, $tariffLbId, $serviceCat->catidx, $price['price'], $nomenclature->count, $serviceLb['service_type']);
                    //                    dd($service_id);
                    if ($service_id) {
                        UserNomenclature::query()->find($nomenclature->id)->update(['synced_at' => now()]);
                    }
                }
            }
        }

        //        Доп поля
        if ($tv24_synchronize) {
            $LBService->setVgroupAddon($vgroup->vgid, "tv24_synchronize", $tv24_synchronize);
            $LBService->setVgroupAddon($vgroup->vgid, "tv24_username", $tv24_username);
        }

        //        Активируем УЗ
        $LBService->blockVgroup($vgroup->vgid, 0);

        return ['error' => 0, 'errorText' => ""];
    }

    private function GetNomenclatures($request_id, $lb_account_id, $lb_agreement)
    {
        if ($request_id) {
            return UserNomenclature::query()
                ->where('request_id', $request_id)
                ->whereNull(['synced_at', 'archived_at'])
                ->orderBy('nomenclature_id')
                ->get();
        } else {
            return UserNomenclature::query()
                ->where('lb_account_id', $lb_account_id)
                ->where('lb_agreement', $lb_agreement)
                ->whereNull(['synced_at', 'archived_at'])
                ->orderBy('nomenclature_id')
                ->get();
        }
    }
}
