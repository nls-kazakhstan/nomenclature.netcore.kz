<?php

namespace App\Http\Services;

use App\Drivers\LanBilling\LbSoapDriver;
use Exception;

class LBService extends LbSoapDriver
{
    public function createMultiTariff($tariffId, $vgroupId)
    {
        try {
            return $this->soapCall('insupdMultitarif', [
                'isInsert' => 1,
                'val' => [
                    'tarid' => $tariffId,
                    'vgid' => $vgroupId,
                    'dtfrom' => now()->format('d.m.Y') . ' 00:00:00',
                    'dtto' => '9999-12-31 23:59:59'
                ],
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function createTariffRasp($tariffId, $vgroupId)
    {
        try {
            return $this->soapCall('insupdTarifsRasp', [
                'val' => [
                    'recordid' => 0,
                    'vgid' => $vgroupId,
                    'id' => 0,
                    'taridold' => 0,
                    'requestby' => 0,
                    'taridnew' => $tariffId,
                    'changetime' => now()->toDateTimeString()
                ],
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getService($tariffId, $tariffDescr)
    {
        try {
            return $this->soapCall('getServiceCategories', [
                'flt' => [
                    'tarid' => $tariffId,
                    'descr' => $tariffDescr
                ],
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getVgroups($lb_account_id)
    {
        try {
            return $this->soapCall('getVgroups', [
                'flt' => [
                    'userid' => $lb_account_id
                ],
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function createService($vgoupId, $tariffId, $serviceID, $price, $count, $service_type, $timefrom)
    {
        $data = [
            'val' => [
                'notusbox' => 1,
                'vgid' => $vgoupId,
                'tarid' => $tariffId,
                'catidx' => $serviceID,
                'mul' => $count,
                'timefrom' => $timefrom
            ],
        ];
        if ($service_type == 'once') {
            $data['val']['modifiers'] = [
                'rate' => 1,
                'rent' => $price
            ];
        }
        try {
            return $this->soapCall('insupdUsboxservice', $data);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function setVgroupAddon($vgoupId, $addonName, $addonValue)
    {
        try {
            return $this->soapCall('setVgroupAddon', [
                'val' => [
                    'type' => null,
                    'agentid' => null,
                    'descr' => '',
                    'idx' => null,
                    'vgid' => $vgoupId,
                    'name' => $addonName,
                    'strvalue' => $addonValue
                ],
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function blockVgroup($vgoupId, $status)
    {
        try {
            return $this->soapCall('insBlkRasp', [
                'val' => [
                    'vgid' => $vgoupId,
                    'blkreq' => $status,
                    'changetime' => now()->addHours(2)->toDateTimeString()
                ],
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
