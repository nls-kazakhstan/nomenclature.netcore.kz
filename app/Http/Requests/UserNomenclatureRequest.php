<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $lb_id
 * @property $lb_agreement
 * @property $tariff
 * @property $nomenclatures
 * @property $iin
 */

class UserNomenclatureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'lb_id' => 'required|numeric|min:1',
            'lb_agreement' => 'required|string',
            'iin' => 'required|numeric|digits:12',
            'tariff' => 'exists:tariffs,name',
            'nomenclatures' => 'required|array',
            'nomenclatures.*.nomenclature_id' => 'exists:nomenclatures,id',
            'nomenclatures.*.count' => 'required|numeric|min:1',
            'nomenclatures.*.payment_type' => 'exists:payment_types,name'
        ];

    }
}
