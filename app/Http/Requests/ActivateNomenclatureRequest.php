<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $request_id
 * @property $lb_id
 * @property $lb_agreement
 * @property $tv24_synchronize
 * @property $tv24_username
 */

class ActivateNomenclatureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'lb_id' => 'required_without:request_id|numeric|min:1',
            'lb_agreement' => 'required_with:lb_id|string',
            'request_id' => 'required_without:lb_id|numeric|min:1',
            'tv24_synchronize' => 'required|boolean',
            'tv24_username' => 'required_if:tv24_synchronize,true|string'
        ];

    }
}
