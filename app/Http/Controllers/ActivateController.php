<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActivateNomenclatureRequest;
use App\Http\Services\NomenclatureService;

class ActivateController extends Controller
{
    /**
     * @param ActivateNomenclatureRequest $activateRequest
     * @param NomenclatureService $nomenclatureService
     * @return array
     */
    public function __invoke(ActivateNomenclatureRequest $activateRequest, NomenclatureService $nomenclatureService): array
    {
        return $nomenclatureService->activate(
            $activateRequest->lb_id,
            $activateRequest->lb_agreement,
            $activateRequest->request_id,
            $activateRequest->tv24_synchronize,
            $activateRequest->tv24_username
        );

    }
}
