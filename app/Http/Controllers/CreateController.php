<?php

namespace App\Http\Controllers;

use App\Http\Services\NomenclatureService;
use App\Http\Requests\UserNomenclatureRequest;

class CreateController extends Controller
{
    /**
     * @param UserNomenclatureRequest $nomenclatureRequest
     * @param NomenclatureService $nomenclatureService
     * @return array
     */
    public function __invoke(
        UserNomenclatureRequest $nomenclatureRequest,
        NomenclatureService $nomenclatureService
    ): array {
        return $nomenclatureService->create(
            $nomenclatureRequest->lb_id,
            $nomenclatureRequest->lb_agreement,
            $nomenclatureRequest->iin,
            $nomenclatureRequest->tariff,
            $nomenclatureRequest->nomenclatures
        );
    }
}
