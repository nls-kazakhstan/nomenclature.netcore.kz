<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
  <title>{{ $code }} — {{ $message }}</title>
  @include('shared.head-icons')
</head>
<body class="antialiased bg-white">

<div class="flex items-center justify-center min-h-screen w-screen">
  <div class="flex flex-col md:flex-row md:items-center">
    <div class="py-8 md:py-4 px-8 md:px-0 md:mr-8 md:pr-8 md:border-r border-gray-100">
      <div class="text-4xl md:text-5xl text-pink-500 font-light leading-none">
        support.netcore.kz
      </div>
    </div>
    <div class="border-t pt-4 md:pt-0 border-gray-100 text-center md:text-left md:border-none">
      <div class="text-xl text-gray-700 font-medium">
        {{ $code }}
      </div>

      <div class="text-gray-400">
        {{ $message }}
      </div>
    </div>
  </div>
</div>

</body>
</html>
