<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <title>{{ env('APP_NAME') }}</title>
  @include('shared.head-icons')
</head>
<body>

@yield('content')

</body>
</html>
