<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNomenclaturePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomenclature_prices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('nomenclature_id')->nullable()->constrained('nomenclatures');
            $table->foreignId('tariff_id')->nullable()->constrained('tariffs');
            $table->foreignId('payment_type_id')->nullable()->constrained('payment_types');
            $table->float('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nomenclature_prices');
    }

}
