<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNomenclaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_nomenclatures', function (Blueprint $table) {
            $table->id();
            $table->foreignId('request_id')->constrained('nomenclature_requests');
            $table->integer('lb_account_id');
            $table->string('lb_agreement');
            $table->string('iin');
            $table->foreignId('tariff_id')->nullable()->constrained('tariffs');
            $table->foreignId('nomenclature_id')->nullable()->constrained('nomenclatures');
            $table->foreignId('payment_type_id')->nullable()->constrained('payment_types');
            $table->integer('count');
            $table->foreignId('price_id')->constrained('nomenclature_prices');
            $table->timestamp('synced_at')->nullable();
            $table->timestamp('archived_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_nomenclatures');
    }

}
