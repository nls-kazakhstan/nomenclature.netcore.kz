<?php

namespace Database\Seeders;

use App\Models\Nomenclature;
use App\Models\NomenclaturePrice;
use App\Models\PaymentType;
use App\Models\Tariff;
use Illuminate\Database\Seeder;

class NomenclaturePriceSeeder extends Seeder
{
    /**
     * @var array
     */

    protected $services_price = [
        'connection' => [ 'purchase' => 40000, 'free' => 0 ],
        'router-mini' => [ 'purchase' => 9000, 'rent' => 350, 'free' => 0 ],
        'router' => [ 'purchase' => 16000, 'rent' => 700, 'free' => 0 ],
        'tv_box' => [ 'purchase' => 16000, 'free' => 0 ],
        'apartment_tv' => [ 'rent' => 2350, 'free' => 0 ],
        'tv_device' => [ 'rent' => 400, 'free' => 0 ],
        'mesh' => [ 'purchase' => 22500, 'free' => 0 ],
        'sks' => [ 'purchase' => 3000, 'free' => 0 ]
    ];

    protected $tariffs_price = [
        'national' => 6000,
        'home' => 9000,
        'family' => 12000,
        'gaming' => 15000,
        'national-plus' => 8450,
        'home-plus' => 11450,
        'family-plus' => 14450,
        'gaming-plus' => 17450,
        'profitable' => 10900,
        'profitable-plus' => 12500,
        'preferential' => 500,
        'preferential-plus' => 2950,
        'optima-100' => 4400,
        'standard-250' => 6900,
        'gamer-500' => 8100
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NomenclaturePrice::query()->truncate();

        foreach ($this->services_price as $nomenclature => $payment_price) {
            foreach ($payment_price as $payment_type => $price) {
                NomenclaturePrice::query()->create([
                    'nomenclature_id' => optional(Nomenclature::query()->where('name', $nomenclature)->first())->id,
                    'tariff_id' => null,
                    'payment_type_id' => optional(PaymentType::query()->where('name', $payment_type)->first())->id,
                    'price' => $price
                ]);
            }
        }

        foreach ($this->tariffs_price as $tariff => $tariff_price) {
            NomenclaturePrice::query()->create([
                'nomenclature_id' => null,
                'tariff_id' => optional(Tariff::query()->where('name', $tariff)->first())->id,
                'payment_type_id' => null,
                'price' => $tariff_price
            ]);
        }
    }

}
