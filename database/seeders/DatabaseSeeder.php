<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        $this->call(TariffSeeder::class);
        $this->call(NomenclatureSeeder::class);
        $this->call(PaymentTypeSeeder::class);
        $this->call(NomenclaturePriceSeeder::class);
        $this->call(BitrixIdSeeder::class);
        $this->call(LbIdSeeder::class);
        Schema::enableForeignKeyConstraints();
    }

}
