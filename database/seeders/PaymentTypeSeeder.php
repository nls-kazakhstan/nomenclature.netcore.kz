<?php

namespace Database\Seeders;

use App\Models\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $data = [
        [ 'name' => 'free', 'title' => 'Бесплатно' ],
        [ 'name' => 'rent', 'title' => 'Аренда' ],
        [ 'name' => 'purchase', 'title' => 'Покупка' ],
        [ 'name' => 'loan2', 'title' => 'Рассрочка на 2 месяца', 'sub_count' => 2 ],
        [ 'name' => 'loan4', 'title' => 'Рассрочка на 4 месяца', 'sub_count' => 4 ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentType::query()->truncate();
        foreach ($this->data as $data) {
            PaymentType::query()->create($data);
        }
    }

}
