<?php

namespace Database\Seeders;

use App\Models\Nomenclature;
use Illuminate\Database\Seeder;

class NomenclatureSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $data = [
        [ 'name' => 'connection', 'title' => 'Подключение' ],
        [ 'name' => 'router-mini', 'title' => 'Роутер 100Мбит' ],
        [ 'name' => 'router', 'title' => 'Роутер 1Гбит' ],
        [ 'name' => 'tv_box', 'title' => 'ТВ приставка' ],
        [ 'name' => 'apartment_tv', 'title' => 'Телевидение в квартиру' ],
        [ 'name' => 'tv_device', 'title' => 'Дополнительная точка ТВ' ],
        [ 'name' => 'mesh', 'title' => 'MESH система' ],
        [ 'name' => 'sks', 'title' => 'СКС' ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Nomenclature::query()->truncate();
        foreach ($this->data as $data) {
            Nomenclature::query()->create($data);
        }
    }

}
