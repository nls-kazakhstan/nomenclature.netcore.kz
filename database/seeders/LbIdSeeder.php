<?php

namespace Database\Seeders;

use App\Models\LbId;
use App\Models\Nomenclature;
use App\Models\PaymentType;
use App\Models\Tariff;
use Illuminate\Database\Seeder;

class LbIdSeeder extends Seeder
{
    /**
     * @var array
     */

    protected $nomenclatures = [
        'connection' => [
            'purchase' => [
                'lb_id' => 0,
                'lb_title' => 'Подключение к сети Интернет',
                'service_type' => 'once'
            ],
            'loan2' => [
                'lb_id' => 82860002,
                'lb_title' => 'Рассрочка на подключение Меганет',
                'service_type' => 'once'
            ],
            'loan4' => [
                'lb_id' => 0,
                'lb_title' => '',
                'service_type' => 'once'
            ],
            'free' => [
                'lb_id' => 0,
                'lb_title' => '',
                'service_type' => 'once'
            ]
        ],
        'router-mini' => [
            'purchase' => [
                'lb_id' => 82860017,
                'lb_title' => 'Продажа роутера',
                'service_type' => 'once'
            ],
            'loan4' => [
                'lb_id' => 82860003,
                'lb_title' => 'Рассрочка на роутер Меганет',
                'service_type' => 'once'
            ],
            'rent' => [
                'lb_id' => 82860005,
                'lb_title' => 'Аренда роутера 100 мбит/с (400 тг)',
                'service_type' => 'periodical'
            ]
        ],
        'router' => [
            'purchase' => [
                'lb_id' => 82860017,
                'lb_title' => 'Продажа роутера',
                'service_type' => 'once'
            ],
            'loan4' => [
                'lb_id' => 82860003,
                'lb_title' => 'Рассрочка на роутер Меганет',
                'service_type' => 'once'
            ],
            'rent' => [
                'lb_id' => 82860006,
                'lb_title' => 'Аренда роутера 1 Гбит/с (700 тг)',
                'service_type' => 'periodical'
            ]
        ],
        'tv_box' => [
            'purchase' => [
                'lb_id' => 0,
                'lb_title' => 'Продажа приставки',
                'service_type' => 'once'
            ],
            'loan4' => [
                'lb_id' => 82860010,
                'lb_title' => 'Аренда приставки (400 тг)',
                'service_type' => 'periodical'
            ]
        ],
        'apartment_tv' => [
            'rent' => [
                'lb_id' => 0,
                'lb_title' => '',
                'service_type' => 'periodical'
            ]
        ],
        'tv_device' => [
            'rent' => [
                'lb_id' => 82860010,
                'lb_title' => 'Аренда приставки (400 тг)',
                'service_type' => 'periodical'
            ]
        ],
        'mesh' => [
            'purchase' => [
                'lb_id' => 0,
                'lb_title' => '',
                'service_type' => 'once'
            ]
        ],
        'sks' => [
            'purchase' => [
                'lb_id' => 0,
                'lb_title' => '',
                'service_type' => 'once'
            ]
        ]
    ];

    protected $tariffs = [
        'national' => ['lb_id' => 1154, 'lb_title' => '*Тариф "Народный"'],
        'home' => ['lb_id' => 1128, 'lb_title' => '*Тариф "Домашний"'],
        'family' => ['lb_id' => 1129, 'lb_title' => '*Тариф "Семейный"'],
        'gaming' => ['lb_id' => 1130, 'lb_title' => '*Тариф "Игровой"'],
        'national-plus' => ['lb_id' => 1133, 'lb_title' => '*Тариф "Народный+" TV на 1 устройство'],
        'home-plus' => ['lb_id' => 1134, 'lb_title' => '*Тариф "Домашний+" TV на 1 устройство'],
        'family-plus' => ['lb_id' => 1155, 'lb_title' => '*Тариф "Семейный+" TV на 1 устройство'],
        'gaming-plus' => ['lb_id' => 1136, 'lb_title' => '*Тариф "Игровой+" TV на 1 устройство'],
        'profitable' => ['lb_id' => 1131, 'lb_title' => '*Тариф "Выгодный"'],
        'profitable-plus' => ['lb_id' => 1137, 'lb_title' => '*Тариф "Выгодный+" TV на 3 устройства'],
        'preferential' => ['lb_id' => 1132, 'lb_title' => '*Тариф "Льготный"'],
        'preferential-plus' => ['lb_id' => 0, 'lb_title' => ''],
        'optima-100' => ['lb_id' => 1156, 'lb_title' => '*OPTIMA 100'],
        'standard-250' => ['lb_id' => 1157, 'lb_title' => '*STANDART 250'],
        'gamer-500' => ['lb_id' => 1158, 'lb_title' => '*GAMER 500']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LbId::query()->truncate();
        foreach ($this->nomenclatures as $nomenclature => $nomenclature_payments) {
            foreach ($nomenclature_payments as $payment_type => $payment_data) {
                LbId::query()->create([
                    'nomenclature_id' => optional(Nomenclature::query()->where('name', $nomenclature)->first())->id,
                    'tariff_id' => null,
                    'payment_type_id' => optional(PaymentType::query()->where('name', $payment_type)->first())->id,
                    'lb_id' => $payment_data['lb_id'],
                    'lb_title' => $payment_data['lb_title'],
                    'service_type' => $payment_data['service_type']
                ]);
            }
        }
        foreach ($this->tariffs as $tariff => $tariff_data) {
            LbId::query()->create([
                'nomenclature_id' => null,
                'tariff_id' => optional(Tariff::query()->where('name', $tariff)->first())->id,
                'payment_type_id' => null,
                'lb_id' => $tariff_data['lb_id'],
                'lb_title' => $tariff_data['lb_title'],
                'service_type' => ''
            ]);
        }
    }
}
