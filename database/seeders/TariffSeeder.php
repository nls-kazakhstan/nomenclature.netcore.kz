<?php

namespace Database\Seeders;

use App\Models\Tariff;
use Illuminate\Database\Seeder;

class TariffSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $data = [
        [ 'name' => 'national', 'title' => 'Народный' ],
        [ 'name' => 'home', 'title' => 'Домашний' ],
        [ 'name' => 'family', 'title' => 'Семейный' ],
        [ 'name' => 'gaming', 'title' => 'Игровой' ],
        [ 'name' => 'national-plus', 'title' => 'Народный +' ],
        [ 'name' => 'home-plus', 'title' => 'Домашний +' ],
        [ 'name' => 'family-plus', 'title' => 'Семейный +' ],
        [ 'name' => 'gaming-plus', 'title' => 'Игровой +' ],
        [ 'name' => 'profitable', 'title' => 'Выгодный' ],
        [ 'name' => 'profitable-plus', 'title' => 'Выгодный +' ],
        [ 'name' => 'preferential', 'title' => 'Льготный' ],
        [ 'name' => 'preferential-plus', 'title' => 'Льготный +' ],
        [ 'name' => 'optima-100', 'title' => 'Оптимальный-100' ],
        [ 'name' => 'standard-250', 'title' => 'Стандартный-250' ],
        [ 'name' => 'gamer-500', 'title' => 'Игровой-500' ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tariff::query()->truncate();
        foreach ($this->data as $data) {
            Tariff::query()->create($data);
        }
    }

}
