<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $data = [
        [
            'name' => 'Astratyan Dmitry',
            'email' => 'astratyandmitry@gmail.com',
        ],
    ];

    /**
     * @return void
     */
    public function run(): void
    {
        foreach ($this->data as $datum) {
            $datum['password'] = bcrypt('qwerty123');

            User::query()->create($datum);
        }
    }

}
