<?php

namespace Database\Seeders;

use App\Models\BitrixId;
use App\Models\Nomenclature;
use App\Models\PaymentType;
use App\Models\Tariff;
use Illuminate\Database\Seeder;

class BitrixIdSeeder extends Seeder
{
    /**
     * @var array
     */

    protected $nomenclatures = [
        'connection' => [
            'purchase' => 737001,
            'loan2' => 741303,
            'loan4' => 736984,
            'free' => 744690,
        ],
        'router-mini' => [
            'purchase' => 794091,
            'loan4' => 793986,
            'rent' => 736938,
        ],
        'router' => [
            'purchase' => 744696,
            'loan4' => 736985,
            'rent' => 739331,
        ],
        'tv_box' => [
            'purchase' => 736940,
            'loan4' => 736986,
        ],

        'apartment_tv' => ['rent' => 793997],
        'tv_device' => ['rent' => 736936],
        'mesh' => ['purchase' => 747717],
        'sks' => ['purchase' => 736941],
    ];

    protected $tariffs = [
        'national' => 736930,
        'home' => 736931,
        'family' => 736932,
        'gaming' => 739329,
        'national-plus' => 736933,
        'home-plus' => 736934,
        'family-plus' => 736935,
        'gaming-plus' => 739330,
        'profitable' => 744674,
        'profitable-plus' => 744678,
        'preferential' => 774764,
        'preferential-plus' => 774774,
        'optima-100' => 793954,
        'standard-250' => 774764,
        'gamer-500' => 774764,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BitrixId::query()->truncate();
        foreach ($this->nomenclatures as $nomenclature => $payment_ids) {
            foreach ($payment_ids as $payment_type => $bitrix_id) {
                BitrixId::query()->create([
                    'nomenclature_id' => optional(Nomenclature::query()->where('name', $nomenclature)->first())->id,
                    'tariff_id' => null,
                    'payment_type_id' => optional(PaymentType::query()->where('name', $payment_type)->first())->id,
                    'bitrix_id' => $bitrix_id
                ]);
            }
        }
        foreach ($this->tariffs as $tariff => $bitrix_id) {
            BitrixId::query()->create([
                'nomenclature_id' => null,
                'tariff_id' => optional(Tariff::query()->where('name', $tariff)->first())->id,
                'payment_type_id' => null,
                'bitrix_id' => $bitrix_id
            ]);
        }
    }

}
